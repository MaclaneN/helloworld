﻿using System;

using Xamarin.Forms;
using TestMe.CustomRenderers;
using TestMe.iOS.CustomRenderers;

[assembly: ExportRenderer(typeof(SquareEntry), typeof(SquareEntryRenderer))]
namespace TestMe.iOS.CustomRenderers
{
    using Xamarin.Forms.Platform.iOS;
    using Xamarin.Forms;
    using UIKit;
    using Foundation;
    using CoreGraphics;

    public class SquareEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                //subscribe to events
            }
            else if (e.OldElement != null) 
            {
                //unscubscribe from events
            }

            if (Control != null)
            {
                Control.Layer.CornerRadius = 0;
                Control.Layer.BorderWidth = 1;
                Control.Layer.BorderColor = UIColor.LightGray.CGColor;
                Control.BackgroundColor = UIColor.White;
                Control.ClipsToBounds = true;

                Control.LeftView = new UIView(new CGRect(0, 0, 12, 0));
                Control.LeftViewMode = UITextFieldViewMode.Always;
                Control.RightView = new UIView(new CGRect(0, 0, 12, 0));
                Control.RightViewMode = UITextFieldViewMode.Always;
            }
        }
    }
}
