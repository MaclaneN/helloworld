﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMe.ViewModels {
    public class CreateToolAndMethodPageViewModel : ViewModelBase {

        private readonly INavigationService _navigationService;

        private DelegateCommand _navigateCommand;
        public DelegateCommand NavigateCommand => _navigateCommand ?? (_navigateCommand = new DelegateCommand(ExecuteNavigateCommand));


        public CreateToolAndMethodPageViewModel(INavigationService navigationService) : base(navigationService) {
            Title = "Create Tool and Method";
            _navigationService = navigationService;

        }
        async void ExecuteNavigateCommand() {
            await _navigationService.NavigateAsync("MasterPage/NavigationPage/MainPage");
        }

    }
}


