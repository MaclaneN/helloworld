﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMe.ViewModels {
    public class CreatePropertyCompletePageViewModel : ViewModelBase {

        private readonly INavigationService _navigationService;

        private DelegateCommand _navigateCommand;
        public DelegateCommand NavigateCommand => _navigateCommand ?? (_navigateCommand = new DelegateCommand(ExecuteNavigateCommand));

        public CreatePropertyCompletePageViewModel(INavigationService navigationService) : base(navigationService) {
            Title = "Complete";
            _navigationService = navigationService;
        }
        
        async void ExecuteNavigateCommand() {
            await _navigationService.NavigateAsync("MasterPage/NavigationPage/MainPage");
        }
    }
}