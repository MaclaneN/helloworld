﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMe.ViewModels {
    public class MasterPageViewModel : ViewModelBase {

        private readonly INavigationService _navigationService;

        public MasterPageViewModel(INavigationService navigationService) : base(navigationService) {
            Title = "Master Page";
            _navigationService = navigationService;
        }

    }
}
