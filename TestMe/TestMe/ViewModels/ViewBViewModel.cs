﻿using Esri.ArcGISRuntime;
using Esri.ArcGISRuntime.Mapping;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMe.ViewModels {
    public class ViewBViewModel : ViewModelBase {

        private Map _testMap;
        public Map TestMap {
            get { return _testMap; }
            set { SetProperty(ref _testMap, value); }
        }

        private readonly INavigationService _navigationService;
        public ViewBViewModel(INavigationService navigationService) : base(navigationService) {
            Title = "View A";
            _navigationService = navigationService;
            TestMap = new Map(Basemap.CreateImagery());
        }
    }
}

