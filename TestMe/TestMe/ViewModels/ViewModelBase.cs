﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestMe.ViewModels {
    public class ViewModelBase : BindableBase, IInitialize, INavigationAware, IDestructible {
        protected INavigationService NavigationService { get; private set; }

        public DelegateCommand<string> NavigateToPageCommand { get; }
        private async void NavigateToPageCommandExecuted(string view) {
            var result = await NavigationService.NavigateAsync($"NavigationPage/{view}");
            if (!result.Success) {
                System.Diagnostics.Debugger.Break();
            }
        }

        private string _title;
        public string Title {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public ViewModelBase(INavigationService navigationService) {
            NavigationService = navigationService;
            NavigateToPageCommand = new DelegateCommand<string>(NavigateToPageCommandExecuted);
        }

        public virtual void Initialize(INavigationParameters parameters) {

        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters) {

        }

        public virtual void OnNavigatedTo(INavigationParameters parameters) {

        }

        public virtual void Destroy() {

        }
    }
}
