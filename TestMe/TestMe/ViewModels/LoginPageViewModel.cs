﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMe.ViewModels {
    public class LoginPageViewModel : ViewModelBase {

        private DelegateCommand _navigateCommand;
        private readonly INavigationService _navigationService;
        public DelegateCommand LoginCommand => _navigateCommand ?? (_navigateCommand = new DelegateCommand(ExecuteNavigateCommand));
        public LoginPageViewModel(INavigationService navigationService) : base(navigationService) {
            Title = "Login Page";
            _navigationService = navigationService;
        }
        async void ExecuteNavigateCommand() {
            await _navigationService.NavigateAsync("MasterPage/NavigationPage/MainPage");
        }
    }
}
