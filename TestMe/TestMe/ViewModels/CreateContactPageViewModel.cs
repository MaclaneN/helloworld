﻿using Esri.ArcGISRuntime;
using Esri.ArcGISRuntime.Mapping;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using TestMe.CustomRenderers;
using Plugin.Segmented.Control;
using Xamarin.Forms;

namespace TestMe.ViewModels {
    public class CreateContactPageViewModel : ViewModelBase {

        private readonly INavigationService _navigationService;

        private ControlTemplate _controlTemplate;
        public ControlTemplate ControlTemplate {
            get { return _controlTemplate; }
            set { SetProperty(ref _controlTemplate, value); }
        }

        private IList<string> _segmentStringSource;
        public IList<string> SegmentStringSource {
            get { return _segmentStringSource; }
            set { SetProperty(ref _segmentStringSource, value); }
        }

        bool originalTemplate = false;
        ControlTemplate AddContactTemplate = new ControlTemplate(typeof(AddContact));
        ControlTemplate CreateContactTemplate = new ControlTemplate(typeof(CreateContact));

        readonly string[] _segmentControlOptions = { "Add Existing Contact", "Create New Contact" };
        public ICommand SegmentChangedCommand { get; }

        public CreateContactPageViewModel(INavigationService navigationService) : base(navigationService) {
            Title = "Create Contact";
            _navigationService = navigationService;
            ControlTemplate = CreateContactTemplate;
            SegmentStringSource = new List<string>(_segmentControlOptions);
            SegmentChangedCommand = new Command(OnSegmentChanged);
        }

        private void OnSegmentChanged(object obj) {
            originalTemplate = !originalTemplate;
            ControlTemplate = (originalTemplate) ? AddContactTemplate : CreateContactTemplate;
        }

        class AddContact : StackLayout {
            public AddContact() {

                Padding = 0;
                Spacing = 12;
                Orientation = StackOrientation.Vertical;

                var searchBar = new SearchBar { Placeholder = "Search items..." };
                Children.Add(searchBar);

                var contactDataTemplate = new DataTemplate(() => {
                    var layoutContainer = new StackLayout() {
                        Spacing = 0,
                        Padding = new Thickness(15, 0, 15, 0),
                        Margin = 0,
                        Orientation = StackOrientation.Horizontal
                    };

                    var verticalContainer = new StackLayout() {
                        Spacing = 0,
                        Padding = 0,
                        Margin = 0,
                        Orientation = StackOrientation.Vertical
                    };
                    layoutContainer.Children.Add(verticalContainer);

                    var Name = new Label() {
                        Text = "TestName",
                        TextColor = Color.FromHex("#0B1824"),
                        FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                    };
                    Name.SetBinding(Label.TextProperty, "FirstName");
                    verticalContainer.Children.Add(Name);

                    var EmailAddress = new Label() {
                        Text = "TestEmailAddress",
                        TextColor = Color.FromHex("#0B1824"),
                        FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                    };
                    EmailAddress.SetBinding(Label.TextProperty, "EmailAddress");
                    verticalContainer.Children.Add(EmailAddress);

                    var Image = new Image() {
                        Source = "plus.png",
                        HeightRequest = 15,
                        WidthRequest = 15,
                        HorizontalOptions = LayoutOptions.EndAndExpand
                    };
                    layoutContainer.Children.Add(Image);

                    return new ViewCell { View = layoutContainer };
                });

                var listView = new ListView() {
                    ItemTemplate = contactDataTemplate
                };

                Children.Add(listView);
            }
        }

        class CreateContact : StackLayout {
            string[] roles = {
                "",
                "Administrator",
                "Authorized Employees",
                "Lessee",
                "Cooperator",
                "Other Authorized Stakeholder",
                "Owner"
            };

            public CreateContact() {
                var contentPresenter = new ContentPresenter();
                Children.Add(contentPresenter);
                Padding = 0;
                Spacing = 12;
                Orientation = StackOrientation.Vertical;

                var layoutSection1Container = new StackLayout() {
                    Spacing = 12,
                    Padding = 0,
                    Orientation = StackOrientation.Vertical
                };
                Children.Add(layoutSection1Container);

                var layoutSection1 = new StackLayout() {
                    Spacing = 8,
                    Padding = 0,
                    Orientation = StackOrientation.Horizontal
                };
                layoutSection1Container.Children.Add(layoutSection1);

                var firstName = new SquareEntry() {
                    Placeholder = "Frist Name",
                    TextColor = Color.FromHex("#0B1824"),
                    Style = (Style)Application.Current.Resources["entryStyle"]
                };
                layoutSection1.Children.Add(firstName);

                var lastName = new SquareEntry() {
                    Placeholder = "Last Name",
                    TextColor = Color.FromHex("#0B1824"),
                    Style = (Style)Application.Current.Resources["entryStyle"]
                };
                layoutSection1.Children.Add(lastName);

                var contactRole = new SquarePicker() {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Title = "Contact Role",
                    TextColor = Color.FromHex("#0B1824"),
                    ItemsSource = roles,
                    Style = (Style)Application.Current.Resources["pickerStyle"]
                };
                layoutSection1Container.Children.Add(contactRole);

                var phoneNumber = new SquareEntry() {
                    Placeholder = "Phone Number",
                    TextColor = Color.FromHex("#0B1824"),
                    Style = (Style)Application.Current.Resources["entryStyle"]
                };
                layoutSection1Container.Children.Add(phoneNumber);

                var emailAddress = new SquareEntry() {
                    Placeholder = "Email Address",
                    TextColor = Color.FromHex("#0B1824"),
                    Style = (Style)Application.Current.Resources["entryStyle"]
                };
                layoutSection1Container.Children.Add(emailAddress);

                var layoutSection2Container = new StackLayout() {
                    Spacing = 0,
                    Orientation = StackOrientation.Horizontal
                };
                Children.Add(layoutSection2Container);

                var isSameAddress = new CheckBox() {
                    IsChecked = false,
                    Color = Color.FromHex("#0F6CFb")
                };

                layoutSection2Container.Children.Add(isSameAddress);

                var sameAsPropertyLabel = new Label() {
                    Text = "Same as Property",
                    TextColor = Color.FromHex("#0B1824"),
                    FontFamily = "Helvetica Light",
                    FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Start
                };
                layoutSection2Container.Children.Add(sameAsPropertyLabel);

                var layoutSection3Container = new StackLayout() {
                    Padding = 0,
                    Spacing = 12,
                    Orientation = StackOrientation.Vertical
                };
                Children.Add(layoutSection3Container);

                var streetAddress = new SquareEntry() {
                    Placeholder = "Street Address",
                    TextColor = Color.FromHex("#0B1824"),
                    Style = (Style)Application.Current.Resources["entryStyle"]
                };
                layoutSection3Container.Children.Add(streetAddress);

                var city = new SquareEntry() {
                    Placeholder = "City",
                    TextColor = Color.FromHex("#0B1824"),
                    Style = (Style)Application.Current.Resources["entryStyle"]
                };
                layoutSection3Container.Children.Add(city);

                var layoutSection3 = new StackLayout() {
                    Padding = 0,
                    Spacing = 8,
                    Orientation = StackOrientation.Horizontal
                };
                layoutSection3Container.Children.Add(layoutSection3);

                var state = new SquareEntry() {
                    Placeholder = "State/Province",
                    TextColor = Color.FromHex("#0B1824"),
                    Style = (Style)Application.Current.Resources["entryStyle"]
                };
                layoutSection3.Children.Add(state);

                var zipCode = new SquareEntry() {
                    Placeholder = "Zip Code",
                    TextColor = Color.FromHex("#0B1824"),
                    Style = (Style)Application.Current.Resources["entryStyle"]
                };
                layoutSection3.Children.Add(zipCode);

                var createContact = new Button() {
                    Margin = new Thickness(0, 15, 0, 0),
                    Text = "Create Contact",
                    BackgroundColor = Color.FromHex("#0F6CFb"),
                    TextColor = Color.FromHex("#FAFAFA"),
                    Style = (Style)Application.Current.Resources["buttonStyle"]
                };
                Children.Add(createContact);

                isSameAddress.CheckedChanged += (sender, e) => {
                    CheckBox checkBox = sender as CheckBox;
                    if (checkBox.IsChecked) {
                        streetAddress.Text = "1234 Sample St";
                        city.Text = "Reston";
                        state.Text = "VA";
                        zipCode.Text = "20170";
                    }
                    else {
                        streetAddress.Text = "";
                        city.Text = "";
                        state.Text = "";
                        zipCode.Text = "";
                    }
                };
            }
        }
    }
}
