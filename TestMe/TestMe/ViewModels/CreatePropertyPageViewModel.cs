﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMe.ViewModels {
    public class CreatePropertyPageViewModel : ViewModelBase {

        private readonly INavigationService _navigationService;

        private DelegateCommand _navigateCommand;
        public DelegateCommand NavigateCommand => _navigateCommand ?? (_navigateCommand = new DelegateCommand(ExecuteNavigateCommand));

        private string[] _landClass;
        public string[] LandClass {
            get { return _landClass; }
            set { SetProperty(ref _landClass, value); }
        }

        string[] landclasses = {
            "",
            "blm land",
            "county or city land",
            "fish and wildlife service",
            "forest service land",
            "military land",
            "other federal land",
            "other public land",
            "private land",
            "state land",
            "tribal land"
        };

        public CreatePropertyPageViewModel(INavigationService navigationService) : base(navigationService) {
            Title = "Create New Property";
            _navigationService = navigationService;
            _landClass = landclasses;
                    
        }
        async void ExecuteNavigateCommand() {
            await _navigationService.NavigateAsync("CreatePropertyRelationshipPage");
        }

    }
}
