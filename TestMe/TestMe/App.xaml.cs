using Prism;
using Prism.Ioc;
using TestMe.ViewModels;
using TestMe.Views;
using Xamarin.Essentials.Interfaces;
using Xamarin.Essentials.Implementation;
using Xamarin.Forms;
using System.ComponentModel;

namespace TestMe {

    [DesignTimeVisible(false)]
    public partial class App {
        public App(IPlatformInitializer initializer) : base(initializer) {
        }

        protected override async void OnInitialized() {
            InitializeComponent();
            Device.SetFlags(new[] { "Shapes_Experimental", "MediaElement_Experimental" });
            await NavigationService.NavigateAsync("LoginPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry) {
            containerRegistry.RegisterSingleton<IAppInfo, AppInfoImplementation>();

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<ViewA, ViewAViewModel>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<MasterPage, MasterPageViewModel>();
            containerRegistry.RegisterForNavigation<ViewB, ViewBViewModel>();
            containerRegistry.RegisterForNavigation<CreateContactPage, CreateContactPageViewModel>();
            containerRegistry.RegisterForNavigation<CreatePropertyPage, CreatePropertyPageViewModel>();
            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>();
            containerRegistry.RegisterForNavigation<CreatePropertyRelationshipPage, CreatePropertyRelationshipPageViewModel>();
            containerRegistry.RegisterForNavigation<CreatePropertyCompletePage, CreatePropertyCompletePageViewModel>();
            containerRegistry.RegisterForNavigation<PropertyDetailPage, PropertyDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<CreateWIDAddPropertyPage, CreateWIDAddPropertyPageViewModel>();
            containerRegistry.RegisterForNavigation<CreateWIDInfoPage, CreateWIDInfoPageViewModel>();
            containerRegistry.RegisterForNavigation<CreateWIDRelationshipPage, CreateWIDRelationshipPageViewModel>();
            containerRegistry.RegisterForNavigation<CreateWIDAssociatedWIDPage, CreateWIDAssociatedWIDPageViewModel>();
            containerRegistry.RegisterForNavigation<CreateWIDCompletePage, CreateWIDCompletePageViewModel>();
            containerRegistry.RegisterForNavigation<CreateResourcePage, CreateResourcePageViewModel>();
            containerRegistry.RegisterForNavigation<CreateSpeciesPage, CreateSpeciesPageViewModel>();
            containerRegistry.RegisterForNavigation<CreateToolAndMethodPage, CreateToolAndMethodPageViewModel>();
        }
    }
}
